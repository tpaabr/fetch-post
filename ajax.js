const $ = document
const url = "https://in-api-treinamento.herokuapp.com/posts"
const but = $.querySelector("#botao");

const myHeaders = {
    "content-Type":"application/json"
}

but.addEventListener("click", () => {
    let nome = $.querySelector("#nome").value;
    let mensagem = $.querySelector("#mensagem").value;

    let novoPost = {
        "post": {
            "name": nome,
            "message": mensagem
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: myHeaders,
        body: JSON.stringify(novoPost)

    }

    fetch(url, fetchConfig).then(console.log)

})



